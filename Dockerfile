# tag::production[]
FROM ruby:2.7.4-alpine
RUN apk add fortune
WORKDIR /code
COPY ["app.rb", "Gemfile", "config.ru", "/code/"]
COPY views /code/views
RUN bundle config set without 'development test'
RUN bundle install
ENV APP_ENV=production

EXPOSE 8080

# <1>
USER 1001:0

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "8080"]
# end::production[]
