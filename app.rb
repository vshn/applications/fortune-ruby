require "sinatra"
require "sinatra/reloader" if development?

set :port, 8080

# tag::router[]
get '/' do
  @version = "1.2-ruby"
  @hostname = `hostname`
  @message = `fortune`
  @number = rand(1000)
  if request.accept? 'text/html'
    erb :fortune
  elsif request.accept? 'application/json'
    obj = { :version => @version,
            :hostname => @hostname,
            :message => @message,
            :number => @number }
    JSON.generate(obj)
  elsif request.accept? 'text/plain'
    "Fortune %s cookie of the day #%d:\n\n%s" % [@version, @number, @message]
  end
end
# end::router[]
