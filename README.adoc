= Fortune in Ruby

This is a Fortune Cookie application written in https://www.ruby-lang.org/en/[Ruby] with the http://sinatrarb.com/[Sinatra] framework.

== Requirements

You need Ruby (version 2 or 3).

[source,shell]
--
$ gem install sinatra
$ gem install sinatra-contrib
$ ruby app.rb
--

Access the application at http://localhost:8080.

== Container

You can use Docker or Podman to build the container image defined in the `Dockerfile`.
